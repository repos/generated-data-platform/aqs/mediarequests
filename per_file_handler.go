package main

import (
	"context"
	"encoding/json"
	"mediarequests/logic"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// PerFileHandler is an HTTP handler for mediarequests/per-file API requests.
type PerFileHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.PerFileLogic
	config  *Config
}

func (s *PerFileHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	// Parameters
	var referer = strings.ToLower(ctx.UserValue("referer").(string))
	var agent = ctx.UserValue("agent").(string)
	var filePath = ctx.UserValue("file-path").(string)
	var granularity = ctx.UserValue("granularity").(string)
	var start, end string

	decodedFilePath, err := url.QueryUnescape(filePath)
	if err != nil {
		s.logger.Error("Error decoding filepath: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, "Invalid granularity", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusInternalServerError)
		ctx.SetBody(problemResp)
		return
	}

	// Parameter validation
	if agent != "all-agents" && agent != "spider" && agent != "user" {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid agent", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}
	if granularity != "daily" && granularity != "monthly" {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid granularity", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}

	if start, err = validateTimestamp(ctx.UserValue("start").(string)); err != nil {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid start timestamp", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}
	if end, err = validateTimestamp(ctx.UserValue("end").(string)); err != nil {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid end timestamp", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)
	pbm, response, pbmStatus := s.logic.ProcessPerFileLogic(c, ctx, agent, referer, decodedFilePath, granularity, start, end, s.session, s.logger)
	defer cancel()

	if pbm != nil {
		problemResp, _ := json.Marshal(pbm)
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, err.Error(), string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(data))
}
