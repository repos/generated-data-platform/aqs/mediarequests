# Mediarequests API

Mediarequests is a public API developed and maintained by the Wikimedia Foundation that serves analytical
data about requested media on Wikipedia and its sister projects. 

Complete documentation can be found on [Wikitech][wikitech].

## Build, Test, & Run

You will need:
- [aqs-docker-test-env](https://gitlab.wikimedia.org/frankie/aqs-docker-test-env) and its associated dependencies

Start up the Dockerized test environment in aqs-docker-test-env, then:

```sh-session
go run .
```
Then, connect to `http://localhost:8080`.

## Prometheus metrics

A resource serving Prometheus request count and latency metrics can be found at `/admin/metrics`.


[wikitech]: https://wikitech.wikimedia.org/wiki/Analytics/AQS/Mediarequests
