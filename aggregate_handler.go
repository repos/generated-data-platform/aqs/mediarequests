package main

import (
	"context"
	"encoding/json"
	"fmt"
	"mediarequests/logic"
	"net/http"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// AggregateHandler is an HTTP handler for mediarequests/aggregate API requests.
type AggregateHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.AggregateLogic
	config  *Config
}

func (s *AggregateHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	// Parameters
	var referer = strings.ToLower(ctx.UserValue("referer").(string))
	var mediaType = strings.ToLower(ctx.UserValue("media-type").(string))
	var agent = ctx.UserValue("agent").(string)
	var granularity = strings.ToLower(ctx.UserValue("granularity").(string))
	var start, end string

	// Parameter validation
	if granularity != "daily" && granularity != "monthly" {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid granularity", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}

	if start, err = validateTimestamp(ctx.UserValue("start").(string)); err != nil {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid start timestamp", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}
	if end, err = validateTimestamp(ctx.UserValue("end").(string)); err != nil {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Invalid start timestamp", string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetStatusCode(http.StatusBadRequest)
		ctx.SetBody(problemResp)
		return
	}

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)
	pbm, response, pbmStatus := s.logic.ProcessAggregateLogic(c, ctx, referer, mediaType, agent, granularity, start, end, s.session, s.logger)
	defer cancel()

	if pbm != nil {
		problemResp, _ := json.Marshal(pbm)
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, err.Error(), string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(data))

}

func validateTimestamp(param string) (string, error) {
	var err error
	var timestamp string

	if len(param) == 8 {
		timestamp = fmt.Sprintf("%s00", param)
	} else {
		timestamp = param
	}

	if _, err = time.Parse("2006010203", timestamp); err != nil {
		return "", err
	}

	return timestamp, nil
}
