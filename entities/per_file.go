package entities

// PerFileResponse represents the API resultset	.
type PerFileResponse struct {
	Items []PerFile `json:"items"`
}

// PerFile represents an entry in the API resultset.
type PerFile struct {
	Referer     string `json:"referer"`
	FilePath    string `json:"file_path"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Agent       string `json:"agent"`
	Requests    int    `json:"requests"`
}
