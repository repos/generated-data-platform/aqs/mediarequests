package logic

import (
	"context"
	"mediarequests/entities"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

type PerFileLogic struct {
}

func (s *PerFileLogic) ProcessPerFileLogic(context context.Context, ctx *fasthttp.RequestCtx, agent, referer, decodedFilePath, granularity, start, end string, session *gocql.Session, rLogger *logger.Logger) (*problem.Problem, entities.PerFileResponse, int) {
	var err error
	var problemData *problem.Problem
	var response = entities.PerFileResponse{Items: make([]entities.PerFile, 0)}

	query := `SELECT spider, user, timestamp FROM "local_group_default_T_mediarequest_per_file".data WHERE "_domain" = 'analytics.wikimedia.org' AND referer = ? AND file_path = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`
	scanner := session.Query(query, referer, decodedFilePath, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var spider int
	var user int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&spider, &user, &timestamp); err != nil {
			rLogger.Log(logger.ERROR, "Query failed: %s", err)
			problemData = aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), string(ctx.Request.URI().RequestURI()))
			return problemData, entities.PerFileResponse{}, http.StatusInternalServerError
		}
		response.Items = append(response.Items, entities.PerFile{
			Referer:     referer,
			FilePath:    decodedFilePath,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Requests:    filterAgent(agent, spider, user),
		})
	}

	if err := scanner.Err(); err != nil {
		rLogger.Log(logger.ERROR, "Error querying database: %s", err)
		problemData = aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), string(ctx.Request.URI().RequestURI()))
		return problemData, entities.PerFileResponse{}, http.StatusInternalServerError
	}

	return problemData, response, 0
}

func filterAgent(agent string, spider int, user int) int {
	if agent == "user" {
		return user
	} else if agent == "spider" {
		return spider
	} else {
		return spider + user
	}
}
