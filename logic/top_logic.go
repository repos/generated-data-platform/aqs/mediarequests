package logic

import (
	"context"
	"encoding/json"
	"mediarequests/entities"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

type TopLogic struct {
}

func (s *TopLogic) ProcessTopLogic(context context.Context, ctx *fasthttp.RequestCtx, referer, mediaType, year, month, day string, session *gocql.Session, rLogger *logger.Logger) (*problem.Problem, entities.TopResponse, int) {
	var err error
	var filesData = []entities.TopFiles{}
	var response = entities.TopResponse{Items: make([]entities.Top, 0)}
	var problemData *problem.Problem
	var filesJSON string

	query := `SELECT "filesJSON" FROM "local_group_default_T_mediarequest_top_files".data WHERE "_domain" = 'analytics.wikimedia.org' AND referer = ? AND media_type = ? AND year = ? AND month = ? AND day = ?`
	if err := session.Query(query, referer, mediaType, year, month, day).WithContext(ctx).Consistency(gocql.One).Scan(&filesJSON); err != nil {
		rLogger.Log(logger.ERROR, "Query failed: %s", err)
		problemData = aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), string(ctx.Request.URI().RequestURI()))
		return problemData, entities.TopResponse{}, http.StatusInternalServerError
	}

	if err = json.Unmarshal([]byte(filesJSON), &filesData); err != nil {
		rLogger.Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problemData = aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), string(ctx.Request.URI().RequestURI()))
		return problemData, entities.TopResponse{}, http.StatusInternalServerError
	}

	var files = []entities.TopFiles{}

	for _, s := range filesData {
		files = append(files, entities.TopFiles{
			FilePath: s.FilePath,
			Requests: s.Requests,
			Rank:     s.Rank,
		})
	}

	response.Items = append(response.Items, entities.Top{
		Referer:   referer,
		MediaType: mediaType,
		Year:      year,
		Month:     month,
		Day:       day,
		Files:     files,
	})

	return problemData, response, 0
}
