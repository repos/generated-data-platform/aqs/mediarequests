package main

import (
	"context"
	"encoding/json"
	"mediarequests/logic"
	"net/http"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// TopHandler is an HTTP handler for mediarequests/top API requests.
type TopHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.TopLogic
	config  *Config
}

func (s *TopHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error
	// Parameters
	var referer = strings.ToLower(ctx.UserValue("referer").(string))
	var mediaType = strings.ToLower(ctx.UserValue("media-type").(string))
	var year = ctx.UserValue("year").(string)
	var month = ctx.UserValue("month").(string)
	var day = ctx.UserValue("day").(string)

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)
	pbm, response, pbmStatus := s.logic.ProcessTopLogic(c, ctx, referer, mediaType, year, month, day, s.session, s.logger)
	defer cancel()

	if pbm != nil {
		problemResp, _ := json.Marshal(pbm)
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, err.Error(), string(ctx.Request.URI().RequestURI())).JSON()
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(data))
}
